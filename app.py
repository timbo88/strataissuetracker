from flask import Flask, render_template, session, g, redirect, url_for, request
from flaskext.mysql import MySQL
from werkzeug.security import generate_password_hash, check_password_hash
import os

app = Flask(__name__)

app.config['DEBUG'] = True
app.config['SECRET_KEY'] = os.urandom(24)

# MySQL configurations
app.config['MYSQL_DATABASE_USER'] = 'root'
app.config['MYSQL_DATABASE_PASSWORD'] = 'Prime123#'
app.config['MYSQL_DATABASE_DB'] = 'strata'
app.config['MYSQL_DATABASE_HOST'] = 'localhost'

flaskmysql = MySQL()
flaskmysql.init_app(app)

def connect_db(flaskmysql):
    con = flaskmysql.connect()
    return con

def get_db(flaskmysql):
    if not hasattr(g, 'mysql_db'):
        g.mysql_db = connect_db(flaskmysql)
    return g.mysql_db

@app.teardown_appcontext
def close_db(error):
    if hasattr(g, 'mysql_db'):
        g.mysql_db.close()

@app.route('/signup', methods=['GET', 'POST'])
def signup():
    if request.method == 'POST':
        con = get_db(flaskmysql)
        cur = con.cursor()
        hashed_password = generate_password_hash(request.form['password'], method='sha256')
        cur.execute('insert into users (email, password_hash) values (%s, %s)', (request.form['email'], hashed_password) )
        con.commit()
#        return '<h2>User Created!</h1>'
        return redirect(url_for('index'))
    return render_template('signup.html')
#    return '<h2>Exit Signup</h2>'


@app.route('/', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        con = get_db(flaskmysql)
        cur = con.cursor()
        email = request.form['email']
        password = request.form['password']
        cur.execute('select email, password_hash from users where email = %s', [email])
        results = cur.fetchall()
        def Convert(tup, di):
            di = dict(tup)
            return di
        dictionary = {}
        results_dict = Convert(results, dictionary)
#        return '<h2> {} </h2>'.format(results_dict[email])
#        return '<h2> {%s} </h2>' % ( results_dict[email] )

        if check_password_hash(results_dict[email], password):
            session['user'] = results_dict[email]
#            return '<h2>Password is correct</h2>'
            return redirect(url_for('index'))
        else:
            return render_template('loginretry.html', user='user')
    else:
        return render_template('login.html')

'''
Session Code

@app.route('/index')
def index():
    user = None
    if 'user' in session:
        user = session['user']
    return render_template('test.html', user=user)
'''

@app.route('/logout')
def logout():
    session.pop('user', None)
    return redirect(url_for('login'))

# Working Phase 1 Code

@app.route('/index', methods=['POST', 'GET'])
def index():
    con = get_db(flaskmysql)
    cur = con.cursor()
    cur.execute('select * from issues')
    results = cur.fetchall()
    #Start - Conversion from Tuples to Dictionary (Copied from Stack Overflow =))
    def Convert(tup, di):
        di = dict(tup)
        return di
    dictionary = {}
    results_dict = Convert(results, dictionary)
    return render_template('home.html', results_dict=results_dict)

@app.route('/add', methods=['GET', 'POST'])
def add():
    if request.method == 'GET':
        con = get_db(flaskmysql)
        cur = con.cursor()
        cur.execute('select * from issues order by issue_number desc limit 1')
        results = cur.fetchall()
        #Start - Conversion from Tuples to Dictionary (Copied from Stack Overflow =))
        def Convert(tup, di):
            di = dict(tup)
            return di
        dictionary = {}
        results_dict = Convert(results, dictionary)
        return render_template('add.html', results_dict=results_dict)
    else:
        name = request.form['name']
        con = get_db(flaskmysql)
        cur = con.cursor()
        cur.execute('insert into issues (issue_description) values (%s)', (name))
        con.commit()
        return redirect(url_for('index'))

@app.route('/edit/<key>', methods=['POST', 'GET'])
def edit(key):
    if request.method == 'GET':
        print("Edit if", key)
        con = get_db(flaskmysql)
        cur = con.cursor()
        cur.execute('select * from issues where issue_number = %s', (int(key)))
        results = cur.fetchall()
        def Convert(tup, di):
            di = dict(tup)
            return di
        dictionary = {}
        results_dict = Convert(results, dictionary)
        return render_template('edit.html', results_dict=results_dict)
    else:
        name = request.form['name']
        print("Edit else=", name, (key))
        print(type(key))
        con = get_db(flaskmysql)
        cur = con.cursor()
        cur.execute('update issues set issue_description = %s where issue_number = %s', (name, (key)))
        con.commit()
        return redirect(url_for('index'))

@app.route('/delete/<key>', methods=['POST','GET'])
def delete(key):
    print("Delete Key is", key)
    con = get_db(flaskmysql)
    cur = con.cursor()
    cur.execute('delete from issues where issue_number = %s', (int(key)))
    con.commit()
    return redirect(url_for('index'))

if __name__ == '__main__':
    app.run(debug=True)
